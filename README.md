# Medium Challenge

Write a program that, given in input from the command line, the name of a CSV file, containing the list of orders of an ecommerce site, and a minimum amount (threashold), calculates the total orders for each buyer and prints all the lines of this buyer if the total of his orders is below the given threshold		

Bonus point: candidate writes unit tests

## Input
The input file is a CSV file with the following structure:

| Id | Article Name | Quantity | Unit Price | Percentage Discount | Buyer |
|-----|--------------|----------|------------|---------------------|------|
|1|Coke|10|1|0|Mario Rossi|
|2|Coke|15|2|0|Luca Neri|
|3|Fanta|5|3|2|Luca Neri|
|4|Water|20|1|10|Mario Rossi|
|5|Fanta|1|4|15|Andrea Bianchi|

